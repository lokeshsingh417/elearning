from django.contrib import admin
from .models import Learner,Subject,Stream,Chapter,Material
# Register your models here.
admin.site.register([Learner,Subject,Stream,Chapter,Material])