from django.shortcuts import render
from rest_framework import viewsets
from .models import Learner,Stream,Subject,Chapter,Material
# Create your views here.
from .serializer import (LearnerSerializer,
						StreamSerializer,
						SubjectSerializer,
						ChapterSerializer,
						MaterialSerializer)
from rest_framework.permissions import IsAuthenticated
class LearnerViewSet(viewsets.ModelViewSet):
	serializer_class = LearnerSerializer
	queryset = Learner.objects.all()
	permission_classes = (IsAuthenticated,)



class StreamViewSet(viewsets.ModelViewSet):
	serializer_class = StreamSerializer
	queryset = Stream.objects.all()
	permission_classes = (IsAuthenticated,)


class SubjectViewSet(viewsets.ModelViewSet):
	serializer_class = SubjectSerializer
	queryset = Subject.objects.all()
	permission_classes = (IsAuthenticated,)
	

class ChapterViewSet(viewsets.ModelViewSet):
	serializer_class = ChapterSerializer
	queryset = Chapter.objects.all()
	permission_classes = (IsAuthenticated,)


class MaterialViewSet(viewsets.ModelViewSet):
	serializer_class = MaterialSerializer
	queryset = Material.objects.all()
	permission_classes = (IsAuthenticated,)


