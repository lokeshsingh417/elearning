from django.db import models
import uuid
from django.contrib.auth.models import User
# Create your models here.
class Learner(models.Model):
	learner_id =models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	name = models.CharField(max_length=100)
	contact_no=models.BigIntegerField()
	avator=models.ImageField(upload_to=f"learner/avator",null=True,blank=True)
	address= models.TextField(null=True,blank=True)
	email_id=models.EmailField()
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Stream(models.Model):
	stream_id = models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4) 
	name = models.CharField(max_length=100)
	user = models.ForeignKey(User,on_delete=models.CASCADE)
	description=models.TextField()
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Subject(models.Model):
	subject_id = models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	name = models.CharField(max_length=100)
	description = models.TextField()
	stream = models.ForeignKey(Stream,on_delete=models.CASCADE)
	user =models.ForeignKey(User,on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Chapter(models.Model):
	chapter_id = models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	name = models.CharField(max_length=100)
	description = models.TextField()
	subject = models.ForeignKey(Subject,on_delete=models.CASCADE)
	user =models.ForeignKey(User,on_delete=models.CASCADE)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name

class Material(models.Model):
	material_id =models.UUIDField(primary_key=True,editable=False,default=uuid.uuid4)
	name = models.CharField(max_length=100)
	description = models.TextField()
	file=models.FileField(upload_to="files")
	chapter = models.ForeignKey(Chapter,on_delete=models.SET_NULL,null=True)
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at=models.DateTimeField(auto_now=True)

	def __str__(self):
		return self.name
