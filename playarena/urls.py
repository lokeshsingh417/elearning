from django.urls import path
from .views import (LearnerViewSet,
					StreamViewSet,
					SubjectViewSet,
					ChapterViewSet,
					MaterialViewSet)
from rest_framework.routers import DefaultRouter

urlpatterns = []


router = DefaultRouter()
router.register('learner', LearnerViewSet, basename='learner')
router.register('stream', StreamViewSet, basename='stream')
router.register('subject', SubjectViewSet, basename='subject')
router.register('chapter', ChapterViewSet, basename='chapter')
router.register('material', MaterialViewSet, basename='material')
urlpatterns = router.urls