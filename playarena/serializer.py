from rest_framework import serializers
from .models import Learner,Stream,Subject,Chapter,Material

class LearnerSerializer(serializers.ModelSerializer):
	class Meta:
		model=Learner
		fields= "__all__"



class StreamSerializer(serializers.ModelSerializer):
	class Meta:
		model=Stream
		fields= "__all__"


class SubjectSerializer(serializers.ModelSerializer):
	class Meta:
		model=Subject
		fields= "__all__"


class ChapterSerializer(serializers.ModelSerializer):
	class Meta:
		model=Chapter
		fields= "__all__"


class MaterialSerializer(serializers.ModelSerializer):
	class Meta:
		model=Material
		fields= "__all__"