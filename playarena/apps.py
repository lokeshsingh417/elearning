from django.apps import AppConfig


class PlayarenaConfig(AppConfig):
    name = 'playarena'
